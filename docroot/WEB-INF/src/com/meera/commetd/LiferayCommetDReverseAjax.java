package com.meera.commetd;

import java.io.IOException;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import com.liferay.util.bridges.mvc.MVCPortlet;
public class LiferayCommetDReverseAjax extends MVCPortlet {
	 private StockPriceEmitter emitter;
		public void startStockUpdates(
				ActionRequest actionRequest, ActionResponse actionResponse)
			throws IOException, PortletException {
			  emitter = new StockPriceEmitter();
			  StockPriceService service=(StockPriceService)getPortletContext().getAttribute(StockPriceService.class.getName());
				 System.out.println("service111"+service);
			 System.out.println("service2222"+service);
			// Register the service as a listener of the emitter
		        emitter.getListeners().add(service);
		        // Start the emitter
		       emitter.start();
		}
		public void stopStockUpdates(
				ActionRequest actionRequest, ActionResponse actionResponse)
			throws IOException, PortletException {
			    //emitter = new StockPriceEmitter();
		        emitter.stop();
		}
}
